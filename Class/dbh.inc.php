<?php

class Dbh {

	private $servername;
	private $username;
	private $password;
	private $dbname;
	private $charset;

	public function connect() {
		$this->servername = "localhost";
//                $this->servername = "10.10.1.2";
		$this->username = "phhsystem";
		$this->password = "cnAfahrenheit5t";
		$this->dbname = "phhsystemindo";
		$this->charset = "utf8";
        
		try {
			$dsn = "mysql:host=".$this->servername.";dbname=".$this->dbname.";charset=".$this->charset;
			$pdo = new PDO($dsn, $this->username, $this->password);
			// $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
		} catch (PDOException $e) {
			echo "Connection failed: ".$e->getMessage();
		}
	}

}

Class SQL extends Dbh{    
    
    protected $sql;
    
    public function __construct($sql) {
        
        $this->sql = $sql;
       
    }
    
    public function getResultRowArray(){
        $resultset = array();
        $sql = $this->sql;
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $resultset = $row;


        }else{
            // do nothing;
            echo "No such table exists.";
            
        }
        
        return $resultset;
        
        
    }
    
    public function getResultOneRowArray(){
        $row =array();
        $sql = $this->sql;
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return $row;
    } 
    
    public function getRowCount(){
        
        $sql = $this->sql;
        
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $number_of_rows = $stmt->fetchColumn(); 
        return $number_of_rows;
    }
    
    public function getUpdate(){
        
        $sql = $this->sql;
        $stmt = $this->connect()->prepare($sql);
        
        if ($stmt->execute()) {
           $result = 'updated'; 
        }else{
            $result = 'update fail'; 
        }
        return $result;
    } 
    public function InsertData(){
        
        $sql = $this->sql;

        $stmt = $this->connect()->prepare($sql);
        
        if ($stmt->execute()) {
           $result = TRUE; 
        }else{
            $result = FALSE; 
        }
        return $result;
    }    

    
}
