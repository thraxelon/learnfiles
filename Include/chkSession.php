<?php

$limit = 900;
$_SESSION['expire'] = time() + $limit; // static expire

if(!isset($_SESSION['un'])){
    header('Refresh: 5;URL=Menu_Login.php');
    echo "Anda belum login. Mengarahkan ke menu login...";
    exit();
}

if(time() > $_SESSION['expire']){ 
    session_unset();
    session_destroy();
    header('Refresh: 5;URL=Menu_Login.php');
    echo "Session has expired, redirecting to login...";
}
?>