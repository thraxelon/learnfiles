<?php
	session_start();
	include("Class/dbh.inc.php");
	include("Class/login.php");
	include("Include/chkSession.php");

	
	global $err;
	if($_POST['user']!=""){
		$nm = $_POST['name'];
		$un = $_POST['user'];
		$ph = $_POST['phrase'];
		$up = $_POST['pass'];
		$rp = $_POST['repass'];
		if($un==""){
			$err = "Harap masukkan Username yang anda inginkan!";
		}elseif($up==""){
			$err = "Harap masukkan Password yang anda inginkan!";
		}elseif($up==$rp) {

			//check if username exists yet
			$ckLogin = new tryLogin($un,$up);
			$rsckLogin = $ckLogin->doLogin();
			if ($rsckLogin == FALSE) {
				$crtLogin = new mkLogin($nm,$un,$up,$ph);
				$rscrtLogin = $crtLogin->createLogin();
				if ($rscrtLogin == TRUE) {
					$_SESSION['headr'] = "Menu_Login.php";
					$_SESSION['retconmsg'] = "User berhasil dibuat, anda akan diarahkan ke menu login.";
					header_remove();
					header("location: redirect.php");
				}else {
					$err = "Terjadi kesalahan teknis, harap coba kembali.";
				}
			}else {
				$err = "User sudah ada, mohon gunakan username lain.";
			}
		}else{
			$err = "Password tidak sama";
		}
		
		}else{
		#echo "huh";
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Create New User</title>
	<style>
		table, th, td {
			padding: 0px;
			white-space: nowrap;

		}
	</style>
</head>
<body> 
	<form action="" method="POST" name="INPUT">
		<table cellpadding="0">
			<tr>
				<td rowspan="1" colspan="0">Name</td>
				<td rowspan="1" colspan="0">:</td>
				<td rowspan="1" colspan="0"><input type="text" name="name"></input></td>
			</tr>
			<tr>
				<td rowspan="1" colspan="0">Username</td>
				<td rowspan="1" colspan="0">:</td>
				<td rowspan="1" colspan="0"><input type="text" name="user"></input></td>
			</tr>
			<tr>
				<td rowspan="1" colspan="0">Phrase</td>
				<td rowspan="1" colspan="0">:</td>
				<td rowspan="1" colspan="0"><input type="text" name="phrase"></input></td>
			</tr>
			<tr>
				<td rowspan="1">Password </td>
				<td rowspan="1" colspan="0">:</td>
				<td rowspan="1"><input type="password" name="pass"></input></td>
			</tr>
			<tr>
				<td rowspan="1">Re-enter Password </td>
				<td rowspan="1">:</td>
				<td rowspan="1"><input type="password" name="repass"></input></td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td>
				<?php echo $err; ?>
				</td>			
			</tr>
			<tr>
				<td><input type="submit" name="Input" value="Submit"></td>
			</tr>
		</table>
	</form>
</body>
</html>
